#!/usr/bin/env python2.7
import json
import sys

"""
first argv -../UNITO_dataset_20170626_curated/data_clinical_patients.json 
second argv - ../UNITO_dataset_20170626_curated/data_clinical_samples.json 
third argv - study name - UNITO_dataset_20170626_curated


../UNITO_dataset_20170626_curated/data_clinical_patients.json ../UNITO_dataset_20170626_curated/data_clinical_samples.json UNITO_dataset_20170626_curated
"""
def read_data():
    file1 = '../europdx-data-public/'+sys.argv[3]+'/'+'data_clinical_patients.txt'
    file2 =  '../europdx-data-public/'+sys.argv[3]+'/'+'data_clinical_samples.txt'
    with open('../IRCCbezduplicit.json', 'r') as jsonFile, open(file1, 'r') as patientFile, open(file2, 'r') as samplesFile:
        dic_patients = {'header': []}
        dic_samples = {'header': []}
        json_data = json.load(jsonFile)
        samples = 0
        for data in json_data['IRCC']:
            patientFile.seek(0)

            for patient_line in patientFile:
                if patient_line[0] == '#':
                    continue

                patient_line_split = patient_line.split('\t')
                if patient_line_split[0] == "PATIENT_ID" or len(patient_line_split) == 1:
                    continue
                if data['Patient ID'] == patient_line_split[5]:
                    dic_patients[data['Model ID']] = patient_line
                    """
                    duplicity
                    if data['Model ID'] not in dic_patients:
                        dic_patients[data['Model ID']] = [patient_line]
                    else:
                        dic_patients[data['Model ID']].append(patient_line)
                    """
                    samplesFile.seek(0)
                    for sample_line in samplesFile:
                        sample_line_split = sample_line.split('\t')
                        if len(sample_line_split) == 1:
                            continue
                        # print("model id: " + data['Model ID'])
                        # print(sample_line_split[3])
                        if patient_line_split[0] == sample_line_split[0]:
                            if sample_line_split[3] in data['Model ID']:
                                samples += 1
                                # dic_samples[data['Model ID']] = sample_line
                                # duplicity
                                if data['Model ID'] not in dic_samples:
                                    dic_samples[data['Model ID']] = sample_line
                                # else:
                                #     dic_samples[data['Model ID']].append(sample_line)
                                #     dic_samples[data['Model ID']] = list(set(dic_samples[data['Model ID']]))


    dic_patients = add_header(dic_patients, '../europdx-data-public/'+sys.argv[3]+'/'+'data_clinical_patients.txt')
    dic_samples = add_header(dic_samples, '../europdx-data-public/'+sys.argv[3]+'/'+'data_clinical_samples.txt')
    print(samples)
    return [dic_patients, dic_samples]


def add_header(dictonary, file):
    with open(file, 'r') as input_file:
        for line in input_file:
            if line[0] == "#":
                dictonary['header'].append(line)
                continue
            split = line.split('\t')
            if split[0] == "PATIENT_ID":
                dictonary['header'].append(line)
                break
    return dictonary

def write_dict(input_dict, filename):
    json_output_data = {}
    for key, value in input_dict.items():
        if key == "header":
            header_value = ""
            for l in value:
                header_value += l
            json_output_data[key] = header_value
            continue
        json_output_data[key] = value
    with open(filename, "w") as output:
        json.dump(json_output_data, output, indent=4)


dicts = read_data()
write_dict(dicts[0], sys.argv[1])
write_dict(dicts[1], sys.argv[2])

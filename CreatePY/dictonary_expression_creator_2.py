#!/usr/bin/env python2.7
import json
import sys

"""
1. ../europdx-data-public/UNITO_dataset_20170626_curated/data_discreteCNA_rtpcr.txt ../UNITO_dataset_20170626_curated/data_discreteCNA_rtpcr.json
2. ../europdx-data-public/UNITO_dataset_20170626_curated/data_discreteCNA_wes.txt ../UNITO_dataset_20170626_curated/data_discreteCNA_wes.json
first argument - input path
second argument - output path 

"""

def read_data(file):
    line = file.readline()
    line = line[:-1]
    ids = line.split('\t')
    ids.pop(0)


    file.seek(0)
    i = 1
    dic = {}

    for lines in file:
        line = lines[:-1]
        line = line.split('\t')
        dic[i] = {'header': line[0]}

        j = 1

        for id in ids:
            dic[i][id] = line[j]
            j += 1

        i += 1

    return dic


with open(sys.argv[1]) as file:
    resultFile = open(sys.argv[2], 'w')

    json.dump(read_data(file), resultFile, indent=4)
    resultFile.close()




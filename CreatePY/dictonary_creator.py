#!/usr/bin/env python2.7
import json
import csv

def read_data():
    with open('IRCC.json', 'r') as jasonFile,open('data_clinical_patients.csv', 'r') as patientFile, open('data_clinical_samples_with_comments.csv', 'r') as samplesFile:

        # result = []
        dic_patients = {'header' : []}
        dic_samples= {'header' : []}
        data = json.load(jasonFile)

        for jason in data['IRCC']:
            patientFile.seek(0)
            patients = csv.reader(patientFile)
            for patient in patients:
                # skipping the first line
                # if patient[0] == 'PATIENT_ID' or patient[0][0] == "#":
                #         dic_patients['header'] += patient + ['\n']

                if jason['Patient ID'] == patient[5]:
                    samplesFile.seek(0)
                    samples = csv.reader(samplesFile)

                    """
                    Checks if the value of cbioportal patient is in the dict
                    """

                    if jason['Model ID'] not in dic_patients:
                        dic_patients[jason['Model ID']] = [patient]
                    else:
                        dic_patients[jason['Model ID']].append(patient)

                    for sample in samples:
                        # if sample[0] == 'PATIENT_ID' or sample[0][0] == "#":
                        #     dic_samples['header'] = dic_samples['header'] + sample + ['\n']
                        if patient[0] == sample[0]:
                            if jason['Model ID'] not in dic_samples:
                                dic_samples[jason['Model ID']] = [sample]
                            else:
                                dic_samples[jason['Model ID']].append(sample)

                            #CSV reading
                            """
                            new_patient = study(jason['Model ID'], jason['Patient ID'], patient[0], jason['Age'], patient[6],
                                  jason['Gender'], jason['Clinical Diagnosis'], jason['Tumor Type'], patient[8])
                            result.append(new_patient)
                            """

    return [dic_patients, dic_samples]

def add_header(dictonary, file):
    with open(file, "r") as inpuut:
        csv_reader = csv.reader(inpuut)
        first = True
        for line in csv_reader:
            if line[0][0] == "#" or line[0] == 'PATIENT_ID':
                if line[len(line)-1] == "":
                    line.pop()
                if first:
                    first = False
                    dictonary['header'] += line
                    continue
                line[0] = '\n' + line[0]
                dictonary['header'] += line
            else:
                return dictonary


def write_csv(result):
    with open('result1.csv', 'w') as resultFile:
        writer = csv.writer(resultFile, dialect='excel')
        writer.writerow(['ModelID', 'PatientID', 'cbioID', 'pdxFinderAge', 'cBioPortalAge', 'gender'])
        # result = list(set(result))
        result.sort()
        print(len(result))
        for i in result:
            writer.writerow([i.pdxFinderID, i.patientID, i.cbioID, i.irccAge, i.cbioAge, i.gender, i.irccClinicalDiagnosis, i.irccTumorType, i.cBioTumorType])


# test version
def write_json_with_data_test(result):
    json_output_data = {'data': []}
    for i in result:
        json_output_data['data'].append({
            'pdxFinderID':i.pdxFinderID,
            'patientID':i.patientID,
            'cBioPortalID':i.cbioID,
            'Clinical Diagnosis':i.irccClinicalDiagnosis,
            'Tumor Type':i.irccTumorType,
            'pdxFinderAge':i.irccAge,
            'cBioPortalAge':i.cbioAge,
            'gender':i.gender,
        })
    with open("json_dic.json",'w') as output:
        json.dump(json_output_data, output,indent=4)


def write_dict(input_dict, filename):
    json_output_data = {}
    for key, values in input_dict.items():

        if values[0][0] == "\n" or values[0][0] == "#":
            json_output_data[key] = '\t'.join(values)
            continue
        for v in values:
            # json_output_data.setdefault(key,[]).append(','.join(v))
            json_output_data[key] = '\t'.join(v)
        # json_output_data['data'].append({key:values})

    with open(filename, "w") as output:
        json.dump(json_output_data, output, indent=4)



dicts = read_data()
dicts[0] = add_header(dicts[0], "data_clinical_patients.csv")
dicts[1] = add_header(dicts[1], "data_clinical_samples_with_comments.csv")
write_dict(dicts[0], "dict_patient.json")
write_dict(dicts[1], "dict_samples.json")

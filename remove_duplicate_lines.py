#!/usr/bin/env python3.7
import sys

# first argument is name of input file
# second is name of output file


lines_seen = set() # holds lines already seen
outfile = open(sys.argv[2], "w")
for line in open(sys.argv[1], "r"):
    if line not in lines_seen: # not a duplicate
        outfile.write(line)
        lines_seen.add(line)
outfile.close()
